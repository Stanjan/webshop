<?php

namespace App\Tests\Functional\Admin\Controller;

use App\DataFixtures\FixtureGroupInterface;
use App\Repository\Admin\UserRepository;
use App\Tests\DoctrineFixturesTest;
use Symfony\Component\DomCrawler\Field\FormField;

/**
 * @covers \App\Admin\Controller\VatRateController
 * @covers \App\Admin\Form\VatRateType
 */
class VatRateControllerTest extends DoctrineFixturesTest
{
    protected function getFixtureGroups(): array
    {
        return [
            FixtureGroupInterface::ADMIN_USER,
            FixtureGroupInterface::WEBSHOP_VAT_RATE,
        ];
    }

    protected function needsDatabaseReset(): bool
    {
        return true;
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::index
     */
    public function testPagination(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $this->client->request('GET', '/vat-rates', [], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Showing 1 to 2 of 2 results', $content);

        $this->client->request('GET', '/vat-rates', [
            'page' => 2,
        ], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('No results found...', $content);
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::index
     */
    public function testSorting(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $this->client->request('GET', '/vat-rates', [], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Name<i class="las la-arrow-down"></i>', $content);
        $this->assertStringContainsString('Percentage<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Created at<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Last updated at<i class="las la-arrows-alt-v"></i>', $content);

        $this->client->request('GET', '/vat-rates', [
            'sort' => 'vr.name',
            'direction' => 'asc',
        ], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Name<i class="las la-arrow-down"></i>', $content);
        $this->assertStringContainsString('Percentage<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Created at<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Last updated at<i class="las la-arrows-alt-v"></i>', $content);

        $this->client->request('GET', '/vat-rates', [
            'sort' => 'vr.percentage',
            'direction' => 'desc',
        ], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Name<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Percentage<i class="las la-arrow-up"></i>', $content);
        $this->assertStringContainsString('Created at<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Last updated at<i class="las la-arrows-alt-v"></i>', $content);

        $this->client->request('GET', '/vat-rates', [
            'sort' => 'vr.createdAt',
            'direction' => 'asc',
        ], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Name<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Percentage<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Created at<i class="las la-arrow-down"></i>', $content);
        $this->assertStringContainsString('Last updated at<i class="las la-arrows-alt-v"></i>', $content);

        $this->client->request('GET', '/vat-rates', [
            'sort' => 'vr.lastUpdatedAt',
            'direction' => 'asc',
        ], [], ['HTTP_HOST' => 'admin.webshop.test']);
        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Name<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Percentage<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Created at<i class="las la-arrows-alt-v"></i>', $content);
        $this->assertStringContainsString('Last updated at<i class="las la-arrow-down"></i>', $content);
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::create
     * @covers \App\Admin\Form\VatRateType
     */
    public function testCreate(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $crawler = $this->client->request('GET', '/vat-rates/new', [], [], ['HTTP_HOST' => 'admin.webshop.test']);

        $form = $crawler->selectButton('Create')->form();

        $this->assertInstanceOf(FormField::class, $nameField = $form['vat_rate[name]']);
        $this->assertInstanceOf(FormField::class, $percentageField = $form['vat_rate[percentage]']);

        $nameField->setValue('Foo');
        $percentageField->setValue('1.111');

        $this->client->submit($form);

        $this->assertResponseStatusCodeSame(302);
        $this->client->followRedirect();

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Foo', $content);
        $this->assertStringContainsString('1.11', $content);
        $this->assertStringContainsString('VAT rate created', $content);
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::create
     * @covers \App\Admin\Form\VatRateType
     */
    public function testCreateInvalid(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $crawler = $this->client->request('GET', '/vat-rates/new', [], [], ['HTTP_HOST' => 'admin.webshop.test']);

        $form = $crawler->selectButton('Create')->form();

        $this->assertInstanceOf(FormField::class, $nameField = $form['vat_rate[name]']);
        $this->assertInstanceOf(FormField::class, $percentageField = $form['vat_rate[percentage]']);

        $nameField->setValue(null);
        $percentageField->setValue('-1');

        $this->client->submit($form);

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Check the form for errors', $content);
        $this->assertStringContainsString('This value should not be blank.', $content);
        $this->assertStringContainsString('This value should be greater than or equal to 0.', $content);

        $nameField->setValue('');
        $percentageField->setValue(null);

        $this->client->submit($form);

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Check the form for errors', $content);
        $this->assertStringContainsString('This value should not be blank.', $content);
        $this->assertStringContainsString('This value is not valid.', $content);
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::edit
     * @covers \App\Admin\Form\VatRateType
     */
    public function testEdit(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $crawler = $this->client->request('GET', '/vat-rates/edit/1', [], [], ['HTTP_HOST' => 'admin.webshop.test']);

        $form = $crawler->selectButton('Save')->form();

        $this->assertInstanceOf(FormField::class, $nameField = $form['vat_rate[name]']);
        $this->assertInstanceOf(FormField::class, $percentageField = $form['vat_rate[percentage]']);

        $nameField->setValue('Foo');
        $percentageField->setValue('1.115');

        $this->client->submit($form);

        $this->assertResponseStatusCodeSame(302);
        $this->client->followRedirect();

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Foo', $content);
        $this->assertStringContainsString('1.12', $content);
        $this->assertStringContainsString('VAT rate saved', $content);
    }

    /**
     * @covers \App\Admin\Controller\VatRateController::edit
     * @covers \App\Admin\Form\VatRateType
     */
    public function testEditInvalid(): void
    {
        /** @var UserRepository $userRepository */
        $userRepository = static::$container->get(UserRepository::class);
        $user = $userRepository->findOneBy(['emailAddress' => 'admin@example.com']);
        $this->client->loginUser($user, 'admin');

        $crawler = $this->client->request('GET', '/vat-rates/edit/1', [], [], ['HTTP_HOST' => 'admin.webshop.test']);

        $form = $crawler->selectButton('Save')->form();

        $this->assertInstanceOf(FormField::class, $nameField = $form['vat_rate[name]']);
        $this->assertInstanceOf(FormField::class, $percentageField = $form['vat_rate[percentage]']);

        $nameField->setValue(null);
        $percentageField->setValue('-1');

        $this->client->submit($form);

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Check the form for errors', $content);
        $this->assertStringContainsString('This value should not be blank.', $content);
        $this->assertStringContainsString('This value should be greater than or equal to 0.', $content);

        $nameField->setValue('');
        $percentageField->setValue(null);

        $this->client->submit($form);

        $content = $this->client->getResponse()->getContent();
        $this->assertIsString($content);
        $this->assertStringContainsString('Check the form for errors', $content);
        $this->assertStringContainsString('This value should not be blank.', $content);
        $this->assertStringContainsString('This value is not valid.', $content);
    }
}
