<?php

namespace App\Tests\Integration\Entity\Admin;

use App\Entity\Admin\User;
use App\Tests\Integration\Entity\EntityConstraintsTest;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @covers \App\Entity\Admin\User
 */
class UserTest extends EntityConstraintsTest
{
    public function testConstraints(): void
    {
        $this->assertConstraintsEquals(User::class, [
            new UniqueEntity('emailAddress'),
        ], [
            'emailAddress' => [
                new Assert\NotBlank(),
                new Assert\Email(),
                new Assert\Length(['max' => 50]),
            ],
            'firstName' => [
                new Assert\NotBlank(),
                new Assert\Length(['max' => 255]),
            ],
            'lastName' => [
                new Assert\NotBlank(),
                new Assert\Length(['max' => 255]),
            ],
            'roles' => [
                new Assert\Choice([
                    'choices' => [User::ROLE_SUPER_ADMIN, User::ROLE_ADMIN],
                    'multiple' => true,
                ]),
            ],
            'plainPassword' => [
                new Assert\NotBlank(['groups' => 'new']),
                new Assert\Length(['min' => 6, 'max' => 128]),
            ],
        ]);
    }
}
