<?php

namespace App\Tests\Integration\Entity;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Mapping\ClassMetadata;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class EntityConstraintsTest extends KernelTestCase
{
    protected function setUp(): void
    {
        static::bootKernel();

        parent::setUp();
    }

    /**
     * Test if all constraints are properly loaded in the Symfony validator.
     *
     * @param string                                $entityClass                   Class of the entity to be tested
     * @param array<int, Constraint>                $expectedClassConstraints
     * @param array<string, array<int, Constraint>> $expectedPropertiesConstraints
     */
    protected function assertConstraintsEquals(string $entityClass, array $expectedClassConstraints, array $expectedPropertiesConstraints): void
    {
        /** @var ValidatorInterface $validator */
        $validator = self::$container->get(ValidatorInterface::class);
        /** @var ClassMetadata $metadata */
        $metadata = $validator->getMetadataFor($entityClass);
        $properties = $metadata->properties;
        $this->assertSame(array_keys($expectedPropertiesConstraints), array_keys($properties));
        $defaultGroups = [Constraint::DEFAULT_GROUP, $metadata->getDefaultGroup()];

        // Class constraints.
        $classConstraints = $metadata->getConstraints();
        $this->assertCount(count($expectedClassConstraints), $classConstraints);
        foreach ($expectedClassConstraints as $index => $expectedClassConstraint) {
            $this->addGroupsToConstraintIfEmpty($expectedClassConstraint, $defaultGroups);
            $this->assertEquals($expectedClassConstraint, $classConstraints[$index]);
        }

        // Property constraints.
        foreach ($expectedPropertiesConstraints as $property => $expectedPropertyConstraints) {
            $propertyConstraints = $properties[$property]->getConstraints();
            $this->assertCount(count($expectedPropertyConstraints), $propertyConstraints);
            foreach ($expectedPropertyConstraints as $index => $expectedPropertyConstraint) {
                $this->addGroupsToConstraintIfEmpty($expectedPropertyConstraint, $defaultGroups);
                $this->assertEquals($expectedPropertyConstraint, $propertyConstraints[$index]);
            }
        }
    }

    /**
     * Adds the given groups to the constraint if no groups were specifically set.
     *
     * @param array<string> $groups
     */
    private function addGroupsToConstraintIfEmpty(Constraint $constraint, array $groups): void
    {
        if ($constraint->groups === [Constraint::DEFAULT_GROUP]) {
            $constraint->groups = $groups;
        }
    }
}
