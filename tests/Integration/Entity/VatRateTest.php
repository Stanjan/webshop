<?php

namespace App\Tests\Integration\Entity;

use App\Entity\VatRate;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @covers \App\Entity\VatRate
 */
class VatRateTest extends EntityConstraintsTest
{
    public function testConstraints(): void
    {
        $this->assertConstraintsEquals(VatRate::class, [], [
            'name' => [
                new Assert\NotBlank(),
            ],
            'percentage' => [
                new Assert\NotBlank(),
                new Assert\GreaterThanOrEqual(0),
            ],
        ]);
    }
}
