<?php

namespace App\Tests\Integration\DataFixtures;

use App\DataFixtures\VatRateFixtures;
use App\Repository\VatRateRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

/**
 * @covers \App\DataFixtures\VatRateFixtures
 * @covers \App\Repository\VatRateRepository
 */
class VatRateFixturesTest extends WebTestCase
{
    protected function setUp(): void
    {
        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);

        // Reset the database.
        $application->run(new StringInput('doctrine:database:drop --force --quiet'));
        $application->run(new StringInput('doctrine:database:create --quiet'));
        $application->run(new StringInput('doctrine:schema:create --quiet'));

        parent::setUp();
    }

    /**
     * @covers \App\DataFixtures\VatRateFixtures::load
     */
    public function testFixturesWithoutReferenceRepository(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::$container->get(EntityManagerInterface::class);

        $this->expectException(\Error::class);
        $this->expectExceptionMessage('Call to a member function setReference() on null');

        $fixtures = new VatRateFixtures();
        $fixtures->load($entityManager);
    }

    /**
     * @covers \App\DataFixtures\VatRateFixtures::load
     */
    public function testFixtures(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::$container->get(EntityManagerInterface::class);
        /** @var VatRateRepository $vatRateRepository */
        $vatRateRepository = static::$container->get(VatRateRepository::class);

        $this->assertSame(0, $vatRateRepository->count([]));

        $referenceRepository = new ReferenceRepository($entityManager);
        $fixtures = new VatRateFixtures();
        $fixtures->setReferenceRepository($referenceRepository);
        $fixtures->load($entityManager);

        $this->assertSame(2, $vatRateRepository->count([]));

        $highRate = $vatRateRepository->findOneBy(['percentage' => 21]);
        $this->assertNotNull($highRate);
        $this->assertSame($referenceRepository->getReference(VatRateFixtures::REFERENCE_HIGH), $highRate);
        $this->assertSame('High', $highRate->getName());
        $this->assertSame(21.0, $highRate->getPercentage());

        $lowRate = $vatRateRepository->findOneBy(['percentage' => 9]);
        $this->assertNotNull($lowRate);
        $this->assertSame($referenceRepository->getReference(VatRateFixtures::REFERENCE_LOW), $lowRate);
        $this->assertSame('Low', $lowRate->getName());
        $this->assertSame(9.0, $lowRate->getPercentage());
    }
}
