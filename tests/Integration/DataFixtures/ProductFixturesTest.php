<?php

namespace App\Tests\Integration\DataFixtures;

use App\DataFixtures\ProductFixtures;
use App\DataFixtures\VatRateFixtures;
use App\Repository\ProductRepository;
use Doctrine\Common\DataFixtures\ReferenceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

/**
 * @covers \App\DataFixtures\ProductFixtures
 * @covers \App\Repository\ProductRepository
 */
class ProductFixturesTest extends WebTestCase
{
    protected function setUp(): void
    {
        $client = static::createClient();

        $application = new Application($client->getKernel());
        $application->setAutoExit(false);

        // Reset the database.
        $application->run(new StringInput('doctrine:database:drop --force --quiet'));
        $application->run(new StringInput('doctrine:database:create --quiet'));
        $application->run(new StringInput('doctrine:schema:create --quiet'));

        parent::setUp();
    }

    /**
     * @covers \App\DataFixtures\ProductFixtures::load
     */
    public function testFixturesWithoutReferenceRepository(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::$container->get(EntityManagerInterface::class);

        $this->expectException(\Error::class);
        $this->expectExceptionMessage('Call to a member function getReference() on null');

        $fixtures = new ProductFixtures();
        $fixtures->load($entityManager);
    }

    /**
     * @covers \App\DataFixtures\ProductFixtures::load
     */
    public function testFixturesWithoutVatRateFixtures(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::$container->get(EntityManagerInterface::class);

        $referenceRepository = new ReferenceRepository($entityManager);
        $fixtures = new ProductFixtures();
        $fixtures->setReferenceRepository($referenceRepository);

        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('Reference to "'.VatRateFixtures::REFERENCE_HIGH.'" does not exist');

        $fixtures->load($entityManager);
    }

    /**
     * @covers \App\DataFixtures\ProductFixtures::load
     * @covers \App\Repository\ProductRepository
     */
    public function testFixtures(): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = static::$container->get(EntityManagerInterface::class);
        /** @var ProductRepository $productRepository */
        $productRepository = static::$container->get(ProductRepository::class);

        $this->assertSame(0, $productRepository->count([]));

        $referenceRepository = new ReferenceRepository($entityManager);
        $vatRateFixtures = new VatRateFixtures();
        $vatRateFixtures->setReferenceRepository($referenceRepository);
        $vatRateFixtures->load($entityManager);
        $fixtures = new ProductFixtures();
        $fixtures->setReferenceRepository($referenceRepository);
        $fixtures->load($entityManager);

        $conceptProduct = $productRepository->findOneBy(['sku' => 'CONCEPT-01']);
        $this->assertNotNull($conceptProduct);
        $this->assertSame($conceptProduct, $referenceRepository->getReference(ProductFixtures::REFERENCE_CONCEPT));

        $publishedProduct = $productRepository->findOneBy(['sku' => 'PUBLISHED-01']);
        $this->assertNotNull($publishedProduct);
        $this->assertSame($publishedProduct, $referenceRepository->getReference(ProductFixtures::REFERENCE_PUBLISHED));

        $archivedProduct = $productRepository->findOneBy(['sku' => 'ARCHIVED-01']);
        $this->assertNotNull($archivedProduct);
        $this->assertSame($archivedProduct, $referenceRepository->getReference(ProductFixtures::REFERENCE_ARCHIVED));

        $this->assertSame(103, $productRepository->count([]));
    }
}
