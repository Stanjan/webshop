<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\StringInput;

abstract class DoctrineFixturesTest extends WebTestCase
{
    protected KernelBrowser $client;

    protected function setUp(): void
    {
        $this->client = static::createClient();

        $this->loadFixtures();

        parent::setUp();
    }

    /**
     * Determines if the tests need a database reset, for example in case of relying on specific entity IDs for routing.
     */
    protected function needsDatabaseReset(): bool
    {
        return false;
    }

    protected function loadFixtures(): void
    {
        $application = new Application($this->client->getKernel());
        $application->setAutoExit(false);

        if ($this->needsDatabaseReset()) {
            // Reset the database.
            $application->run(new StringInput('doctrine:database:drop --force --quiet'));
            $application->run(new StringInput('doctrine:database:create --quiet'));
            $application->run(new StringInput('doctrine:schema:create --quiet'));
        }

        $command = 'doctrine:fixtures:load -n --quiet';

        foreach ($this->getFixtureGroups() as $group) {
            $command .= ' --group='.$group;
        }

        $application->run(new StringInput($command));
    }

    /**
     * @return array<string>
     */
    abstract protected function getFixtureGroups(): array;
}
