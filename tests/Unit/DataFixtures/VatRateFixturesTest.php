<?php

namespace App\Tests\Unit\DataFixtures;

use App\DataFixtures\FixtureGroupInterface;
use App\DataFixtures\VatRateFixtures;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DataFixtures\VatRateFixtures
 */
class VatRateFixturesTest extends TestCase
{
    public function testConstruct(): void
    {
        $fixtures = new VatRateFixtures();

        $this->assertInstanceOf(FixtureGroupInterface::class, $fixtures);
    }

    /**
     * @covers \App\DataFixtures\VatRateFixtures::getGroups
     */
    public function testGetGroups(): void
    {
        $this->assertSame([
            FixtureGroupInterface::WEBSHOP,
            FixtureGroupInterface::WEBSHOP_PRODUCT,
            FixtureGroupInterface::WEBSHOP_VAT_RATE,
        ], VatRateFixtures::getGroups());
    }
}
