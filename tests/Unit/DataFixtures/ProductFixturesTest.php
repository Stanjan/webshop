<?php

namespace App\Tests\Unit\DataFixtures;

use App\DataFixtures\FixtureGroupInterface;
use App\DataFixtures\ProductFixtures;
use App\DataFixtures\VatRateFixtures;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\DataFixtures\ProductFixtures
 */
class ProductFixturesTest extends TestCase
{
    public function testConstruct(): void
    {
        $fixtures = new ProductFixtures();

        $this->assertInstanceOf(FixtureGroupInterface::class, $fixtures);
        $this->assertInstanceOf(DependentFixtureInterface::class, $fixtures);
    }

    /**
     * @covers \App\DataFixtures\ProductFixtures::getGroups
     */
    public function testGetGroups(): void
    {
        $this->assertSame([
            FixtureGroupInterface::ADMIN,
            FixtureGroupInterface::WEBSHOP,
            FixtureGroupInterface::WEBSHOP_PRODUCT,
        ], ProductFixtures::getGroups());
    }

    /**
     * @covers \App\DataFixtures\ProductFixtures::getDependencies
     */
    public function testGetDependencies(): void
    {
        $fixtures = new ProductFixtures();

        $this->assertSame([
            VatRateFixtures::class,
        ], $fixtures->getDependencies());
    }
}
