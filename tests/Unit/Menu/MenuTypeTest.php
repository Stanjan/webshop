<?php

namespace App\Tests\Unit\Menu;

use App\Menu\MenuBuilder;
use App\Menu\MenuInterface;
use App\Menu\MenuItemInterface;
use App\Menu\MenuTypeInterface;
use PHPUnit\Framework\TestCase;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\Routing\RouterInterface;

abstract class MenuTypeTest extends TestCase
{
    use ProphecyTrait;

    /**
     * @param array<MenuItemDescription> $expectedItems
     */
    protected function assertBuild(MenuTypeInterface $type, array $expectedItems): void
    {
        $routerProphecy = $this->prophesize(RouterInterface::class);

        $this->fillUriGenerateProphecy($routerProphecy, $expectedItems);

        $builder = new MenuBuilder($routerProphecy->reveal());
        $type->build($builder);

        $menu = $builder->getMenu();

        $this->assertMenuItems($expectedItems, $menu);
    }

    /**
     * @param ObjectProphecy<RouterInterface> $prophecy
     * @param array<MenuItemDescription>      $expectedItems
     */
    private function fillUriGenerateProphecy(ObjectProphecy $prophecy, array $expectedItems): void
    {
        foreach ($expectedItems as $item) {
            if ($item->getRoute() !== null) {
                $prophecy->generate($item->getRoute(), $item->getRouteParams())->shouldBeCalledTimes(1)->willReturn($item->getUri());
            }
            $this->fillUriGenerateProphecy($prophecy, $item->getChildren());
        }
    }

    /**
     * @param array<MenuItemDescription> $expectedItems
     */
    private function assertMenuItems(array $expectedItems, MenuInterface $menu): void
    {
        foreach ($expectedItems as $item) {
            /** @var MenuItemInterface $menuItem */
            $menuItem = $menu[$item->getIdentifier()];

            $this->assertSame($item->getIdentifier(), $menuItem->getIdentifier());
            $this->assertSame($item->getLabel(), $menuItem->getLabel());
            $this->assertSame($item->getUri(), $menuItem->getUri());
            $this->assertFalse($menuItem->isActive());
            $this->assertSame($item->getTarget(), $menuItem->getTarget());
            $this->assertSame($item->getIcon(), $menuItem->getIcon());
            $this->assertSame($item->getTranslationDomain(), $menuItem->getTranslationDomain());
            $this->assertCount(count($item->getChildren()), $menuItem->getChildren());

            $this->assertMenuItems($item->getChildren(), $menuItem);
        }
    }
}
