<?php

namespace App\Tests\Unit\Menu;

/**
 * Description of a menu item instance that can be used for testing \App\Menu\MenuItem::class instances.
 */
class MenuItemDescription
{
    /**
     * @param array<mixed>               $routeParams
     * @param array<MenuItemDescription> $children
     */
    public function __construct(
        private string $identifier,
        private string $label,
        private string $uri,
        private ?string $route = null,
        private array $routeParams = [],
        private ?string $target = null,
        private ?string $icon = null,
        private ?string $translationDomain = null,
        private array $children = [],
    ) {}

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @return array<mixed>
     */
    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getTranslationDomain(): ?string
    {
        return $this->translationDomain;
    }

    /**
     * @return MenuItemDescription[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }
}