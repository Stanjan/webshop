<?php

namespace App\Tests\Unit\Admin\Menu;

use App\Admin\Menu\AdminType;
use App\Entity\Admin\User;
use App\Tests\Unit\Menu\MenuItemDescription;
use App\Tests\Unit\Menu\MenuTypeTest;
use Prophecy\PhpUnit\ProphecyTrait;
use Symfony\Component\Security\Core\Security;

/**
 * @covers \App\Admin\Menu\AdminType
 */
class AdminTypeTest extends MenuTypeTest
{
    use ProphecyTrait;

    /**
     * @covers \App\Admin\Menu\AdminType::getKey
     */
    public function testKey(): void
    {
        $this->assertSame('admin', AdminType::getKey());
    }

    /**
     * @covers \App\Admin\Menu\AdminType::build
     */
    public function testBuildAdminUser(): void
    {
        $securityProphecy = $this->prophesize(Security::class);
        $securityProphecy->isGranted(User::ROLE_ADMIN)->shouldBeCalledTimes(1)->willReturn(true);

        $this->assertBuild(new AdminType($securityProphecy->reveal()), array_merge($this->getDefaultExpectedItems(), [
            new MenuItemDescription(
                'admin_user',
                'menu.admin_user',
                '/admin-users',
                'admin_admin_user_index',
                [],
                null,
                'las la-user-shield',
                'messages',
            ),
        ]));
    }

    /**
     * @covers \App\Admin\Menu\AdminType::build
     */
    public function testBuildDefaultUser(): void
    {
        $securityProphecy = $this->prophesize(Security::class);
        $securityProphecy->isGranted(User::ROLE_ADMIN)->shouldBeCalledTimes(1)->willReturn(false);

        $this->assertBuild(new AdminType($securityProphecy->reveal()), $this->getDefaultExpectedItems());
    }

    /**
     * @return array<MenuItemDescription>
     */
    private function getDefaultExpectedItems(): array
    {
        return [
            new MenuItemDescription(
                'dashboard',
                'menu.dashboard',
                '/',
                'admin_dashboard',
                [],
                null,
                'las la-home',
                'messages',
            ),
            new MenuItemDescription(
                'master_data',
                'menu.master_data',
                '#',
                null,
                [],
                null,
                'las la-tools',
                'messages',
                [
                    new MenuItemDescription(
                        'vat_rate',
                        'menu.vat_rate',
                        '/vat-rates',
                        'admin_vat_rate_index',
                        [],
                        null,
                        'las la-percent',
                        'messages',
                    ),
                ],
            ),
        ];
    }
}
