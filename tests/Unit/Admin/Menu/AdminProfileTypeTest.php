<?php

namespace App\Tests\Unit\Admin\Menu;

use App\Admin\Menu\AdminProfileType;
use App\Tests\Unit\Menu\MenuItemDescription;
use App\Tests\Unit\Menu\MenuTypeTest;

/**
 * @covers \App\Admin\Menu\AdminProfileType
 */
class AdminProfileTypeTest extends MenuTypeTest
{
    /**
     * @covers \App\Admin\Menu\AdminProfileType::getKey
     */
    public function testKey(): void
    {
        $this->assertSame('admin_profile', AdminProfileType::getKey());
    }

    /**
     * @covers \App\Admin\Menu\AdminProfileType::build
     */
    public function testBuild(): void
    {
        $this->assertBuild(new AdminProfileType(), [
            new MenuItemDescription(
                'profile',
                'menu.profile',
                '/profile',
                'admin_profile_edit',
                [],
                null,
                'las la-user',
                'messages',
            ),
            new MenuItemDescription(
                'logout',
                'menu.logout',
                '/logout',
                'admin_logout',
                [],
                null,
                'las la-sign-out-alt',
                'messages',
            ),
        ]);
    }
}
