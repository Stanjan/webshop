<?php

namespace App\Tests\Unit\Entity;

use App\ActionLog\LoggableObjectInterface;
use App\Entity\Product;
use App\Entity\VatRate;
use App\Timestampable\TimestampableInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Entity\Product
 */
class ProductTest extends TestCase
{
    public function testConstruct(): void
    {
        $product = new Product();

        $this->assertSame(null, $product->getId());
        $this->assertSame(Product::STATUS_CONCEPT, $product->getStatus());
        $this->assertFalse($product->canPreorder());

        $this->assertInstanceOf(TimestampableInterface::class, $product);
        $this->assertInstanceOf(LoggableObjectInterface::class, $product);

        $this->assertSame([], $product->getNonLoggableProperties());
    }

    public function testSetters(): void
    {
        $product = new Product();

        $product->setSku('foo');
        $this->assertSame('foo', $product->getSku());

        $product->setBarcode('bar');
        $this->assertSame('bar', $product->getBarcode());
        $product->setBarcode(null);
        $this->assertNull($product->getBarcode());

        $product->setName('product_name');
        $this->assertSame('product_name', $product->getName());

        $product->setDescription('product_description');
        $this->assertSame('product_description', $product->getDescription());
        $product->setDescription(null);
        $this->assertNull($product->getDescription());

        $product->setSlug('product_slug');
        $this->assertSame('product_slug', $product->getSlug());

        $product->setPurchasePrice(1);
        $this->assertSame(1.0, $product->getPurchasePrice());
        $product->setPurchasePrice(1.11);
        $this->assertSame(1.11, $product->getPurchasePrice());

        $product->setSalesPrice(2);
        $this->assertSame(2.0, $product->getSalesPrice());
        $product->setSalesPrice(2.22);
        $this->assertSame(2.22, $product->getSalesPrice());

        $product->setStatus('foo_bar');
        $this->assertSame('foo_bar', $product->getStatus());
        $product->setStatus(Product::STATUS_CONCEPT);
        $this->assertSame(Product::STATUS_CONCEPT, $product->getStatus());
        $product->setStatus(Product::STATUS_PUBLISHED);
        $this->assertSame(Product::STATUS_PUBLISHED, $product->getStatus());
        $product->setStatus(Product::STATUS_ARCHIVED);
        $this->assertSame(Product::STATUS_ARCHIVED, $product->getStatus());

        $availableFromDate = new \DateTimeImmutable('11-11-2011');
        $product->setAvailableFrom($availableFromDate);
        $this->assertSame($availableFromDate, $product->getAvailableFrom());
        $product->setAvailableFrom(null);
        $this->assertNull($product->getAvailableFrom());

        $availableToDate = new \DateTimeImmutable('1-1-2001');
        $product->setAvailableTo($availableToDate);
        $this->assertSame($availableToDate, $product->getAvailableTo());
        $product->setAvailableTo(null);
        $this->assertNull($product->getAvailableTo());

        $product->setCanPreorder(true);
        $this->assertTrue($product->canPreorder());

        $product->setWeight(11);
        $this->assertSame(11.0, $product->getWeight());
        $product->setWeight(11.11);
        $this->assertSame(11.11, $product->getWeight());
        $product->setWeight(null);
        $this->assertNull($product->getWeight());

        $product->setStock(6);
        $this->assertSame(6.0, $product->getStock());
        $product->setStock(6.66);
        $this->assertSame(6.66, $product->getStock());

        $vatRate = new VatRate();
        $product->setVatRate($vatRate);
        $this->assertSame($vatRate, $product->getVatRate());
    }
}
