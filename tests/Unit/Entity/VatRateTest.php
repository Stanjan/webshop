<?php

namespace App\Tests\Unit\Entity;

use App\ActionLog\LoggableObjectInterface;
use App\Entity\VatRate;
use App\Timestampable\TimestampableInterface;
use PHPUnit\Framework\TestCase;

/**
 * @covers \App\Entity\VatRate
 */
class VatRateTest extends TestCase
{
    public function testConstruct(): void
    {
        $vatRate = new VatRate();

        $this->assertSame(null, $vatRate->getId());

        $this->assertInstanceOf(TimestampableInterface::class, $vatRate);
        $this->assertInstanceOf(LoggableObjectInterface::class, $vatRate);

        $this->assertSame([], $vatRate->getNonLoggableProperties());
    }

    public function testSetters(): void
    {
        $vatRate = new VatRate();

        $vatRate->setName('foo');
        $this->assertSame('foo', $vatRate->getName());

        $vatRate->setPercentage(11);
        $this->assertSame(11.0, $vatRate->getPercentage());
    }
}
