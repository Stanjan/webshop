<?php

namespace App\Admin\Controller;

use App\Admin\Form\VatRateType;
use App\Entity\VatRate;
use App\Repository\VatRateRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Controller for managing \App\Entity\VatRate::class instances.
 */
class VatRateController extends AbstractController
{
    public function __construct(
        private VatRateRepository $vatRateRepository,
        private EntityManagerInterface $entityManager,
        private TranslatorInterface $translator,
        private PaginatorInterface $paginator,
    ) {}

    #[Route('/vat-rates', name: 'admin_vat_rate_index')]
    public function index(Request $request): Response
    {
        $queryBuilder = $this->vatRateRepository->createQueryBuilder('vr');

        $pagination = $this->paginator->paginate(
            $queryBuilder,
            $request->query->getInt('page', 1),
            10,
            [
                PaginatorInterface::DEFAULT_SORT_FIELD_NAME => 'vr.name',
                PaginatorInterface::DEFAULT_SORT_DIRECTION => 'ASC',
                PaginatorInterface::SORT_FIELD_ALLOW_LIST => ['vr.name', 'vr.percentage', 'vr.createdAt', 'vr.lastUpdatedAt'],
            ],
        );

        return $this->render('admin/vat_rate/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    #[Route('/vat-rates/new', name: 'admin_vat_rate_new')]
    public function create(Request $request): Response
    {
        $vatRate = new VatRate();

        $form = $this->createForm(VatRateType::class, $vatRate);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($vatRate);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('vat_rate.created'));

            return $this->redirectToRoute('admin_vat_rate_edit', [
                'id' => $vatRate->getId(),
            ]);
        } elseif ($form->isSubmitted()) {
            $this->addFlash('danger', $this->translator->trans('form.invalid'));
        }

        return $this->render('admin/vat_rate/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/vat-rates/edit/{id}', name: 'admin_vat_rate_edit')]
    public function edit(Request $request, VatRate $vatRate): Response
    {
        $form = $this->createForm(VatRateType::class, $vatRate);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($vatRate);
            $this->entityManager->flush();

            $this->addFlash('success', $this->translator->trans('vat_rate.saved'));

            return $this->redirectToRoute('admin_vat_rate_index');
        } elseif ($form->isSubmitted()) {
            $this->addFlash('danger', $this->translator->trans('form.invalid'));
        }

        return $this->render('admin/vat_rate/edit.html.twig', [
            'vat_rate' => $vatRate,
            'form' => $form->createView(),
        ]);
    }
}
