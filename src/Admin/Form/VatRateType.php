<?php

namespace App\Admin\Form;

use App\Entity\VatRate;
use NumberFormatter;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Create/edit form type for \App\Entity\VatRate::class.
 */
class VatRateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'vat_rate.name',
                'required' => true,
                'empty_data' => '',
            ])
            ->add('percentage', PercentType::class, [
                'label' => 'vat_rate.percentage',
                'type' => 'integer',
                'scale' => 2,
                'rounding_mode' => NumberFormatter::ROUND_HALFUP,
                'required' => true,
                'empty_data' => 0.0,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => VatRate::class,
        ]);
    }
}
