<?php

namespace App\Admin\Menu;

use App\Entity\Admin\User;
use App\Menu\AbstractMenuType;
use App\Menu\MenuBuilder;
use Symfony\Component\Security\Core\Security;

/**
 * Menu type for the default navbar in the admin.
 */
class AdminType extends AbstractMenuType
{
    public function __construct(
        private Security $security,
    ) {}

    public function build(MenuBuilder $builder): void
    {
        $isAdmin = $this->security->isGranted(User::ROLE_ADMIN);

        $builder->add('dashboard', [
            'label' => 'menu.dashboard',
            'route' => 'admin_dashboard',
            'active_pattern' => '/admin_dashboard/',
            'icon' => 'las la-home',
        ]);

        if ($isAdmin) {
            $builder->add('admin_user', [
                'label' => 'menu.admin_user',
                'route' => 'admin_admin_user_index',
                'active_pattern' => '/admin_admin_user_(.*)/',
                'icon' => 'las la-user-shield',
            ]);
        }

        $builder->add($builder->create('master_data', [
            'label' => 'menu.master_data',
            'icon' => 'las la-tools',
            'uri' => '#',
        ])
            ->add('vat_rate', [
                'label' => 'menu.vat_rate',
                'route' => 'admin_vat_rate_index',
                'active_pattern' => '/admin_vat_rate_(.*)/',
                'icon' => 'las la-percent',
            ])
        );
    }
}
