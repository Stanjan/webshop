<?php

namespace App\Admin\Chart;

class BarChart
{
    /**
     * @param array<string>           $labels
     * @param array<array<int|float>> $bars
     */
    public function __construct(
        private array $labels,
        private array $bars,
    ) {}

    /**
     * @return array<string>
     */
    public function getLabels(): array
    {
        return $this->labels;
    }

    /**
     * @return array<array<int|float>>
     */
    public function getBars(): array
    {
        return $this->bars;
    }
}
