<?php

namespace App\Entity;

use App\ActionLog\LoggableObjectInterface;
use App\Repository\ProductRepository;
use App\Timestampable\TimestampableEntity;
use App\Timestampable\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Main product entity.
 *
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 * @UniqueEntity("sku")
 * @UniqueEntity("barcode")
 * @UniqueEntity("slug")
 */
class Product implements TimestampableInterface, LoggableObjectInterface
{
    use TimestampableEntity;

    const STATUS_CONCEPT = 'concept';
    const STATUS_PUBLISHED = 'published';
    const STATUS_ARCHIVED = 'archived';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * Stock keeping unit.
     *
     * @ORM\Column(type="string", length=12, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=8, max=12)
     */
    private string $sku;

    /**
     * EAN-13 barcode.
     *
     * @ORM\Column(type="string", length=13, nullable=true, unique=true)
     * @Assert\Length(max=13)
     */
    private ?string $barcode;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private string $name;

    /**
     * Richtext description.
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $description;

    /**
     * This slug does not include the leading slash.
     *
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(max=100)
     */
    private string $slug;

    /**
     * The price the product is purchased for.
     *
     * @ORM\Column(type="float")
     * @Assert\GreaterThanOrEqual(0)
     */
    private float $purchasePrice;

    /**
     * The base price the product is sold for.
     *
     * @ORM\Column(type="float")
     * @Assert\GreaterThan(0)
     */
    private float $salesPrice;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     */
    private string $status = self::STATUS_CONCEPT;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $availableFrom;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Assert\GreaterThanOrEqual(propertyPath="availableFrom")
     */
    private ?\DateTimeInterface $availableTo;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $canPreorder = false;

    /**
     * @ORM\Column(type="float", nullable=true)
     * @Assert\GreaterThanOrEqual(0)
     */
    private ?float $weight;

    /**
     * @ORM\Column(type="float")
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private float $stock;

    /**
     * @ORM\ManyToOne(targetEntity=VatRate::class)
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank()
     */
    private VatRate $vatRate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSku(): string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getPurchasePrice(): float
    {
        return $this->purchasePrice;
    }

    public function setPurchasePrice(float $purchasePrice): self
    {
        $this->purchasePrice = $purchasePrice;

        return $this;
    }

    public function getSalesPrice(): float
    {
        return $this->salesPrice;
    }

    public function setSalesPrice(float $salesPrice): self
    {
        $this->salesPrice = $salesPrice;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getAvailableFrom(): ?\DateTimeInterface
    {
        return $this->availableFrom;
    }

    public function setAvailableFrom(?\DateTimeInterface $availableFrom): self
    {
        $this->availableFrom = $availableFrom;

        return $this;
    }

    public function getAvailableTo(): ?\DateTimeInterface
    {
        return $this->availableTo;
    }

    public function setAvailableTo(?\DateTimeInterface $availableTo): self
    {
        $this->availableTo = $availableTo;

        return $this;
    }

    public function canPreorder(): bool
    {
        return $this->canPreorder;
    }

    public function setCanPreorder(bool $canPreorder): self
    {
        $this->canPreorder = $canPreorder;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getStock(): float
    {
        return $this->stock;
    }

    public function setStock(float $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getNonLoggableProperties(): array
    {
        return [];
    }

    public function getVatRate(): VatRate
    {
        return $this->vatRate;
    }

    public function setVatRate(VatRate $vatRate): self
    {
        $this->vatRate = $vatRate;

        return $this;
    }
}
