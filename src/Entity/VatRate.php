<?php

namespace App\Entity;

use App\ActionLog\LoggableObjectInterface;
use App\Repository\VatRateRepository;
use App\Timestampable\TimestampableEntity;
use App\Timestampable\TimestampableInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=VatRateRepository::class)
 */
class VatRate implements TimestampableInterface, LoggableObjectInterface
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private string $name;

    /**
     * The full percentage of this vat rate (21% is 21.0, NOT 0.21).
     *
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     */
    private float $percentage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPercentage(): float
    {
        return $this->percentage;
    }

    public function setPercentage(float $percentage): self
    {
        $this->percentage = $percentage;

        return $this;
    }

    public function getNonLoggableProperties(): array
    {
        return [];
    }
}
