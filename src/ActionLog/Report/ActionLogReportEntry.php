<?php

namespace App\ActionLog\Report;

use App\Entity\ActionLog;

/**
 * Collection for ActionLog::class grouped by action in a given datetime period.
 */
class ActionLogReportEntry
{
    /**
     * @param array<ActionLog> $actionLogsCreate
     * @param array<ActionLog> $actionLogsEdit
     * @param array<ActionLog> $actionLogsDelete
     */
    public function __construct(
        private \DateTimeInterface $dateFrom,
        private \DateTimeInterface $dateTo,
        private array $actionLogsCreate,
        private array $actionLogsEdit,
        private array $actionLogsDelete,
    ) {}

    public function getDateFrom(): \DateTimeInterface
    {
        return $this->dateFrom;
    }

    public function getDateTo(): \DateTimeInterface
    {
        return $this->dateTo;
    }

    /**
     * @return array<ActionLog>
     */
    public function getActionLogsCreate(): array
    {
        return $this->actionLogsCreate;
    }

    /**
     * @return array<ActionLog>
     */
    public function getActionLogsEdit(): array
    {
        return $this->actionLogsEdit;
    }

    /**
     * @return array<ActionLog>
     */
    public function getActionLogsDelete(): array
    {
        return $this->actionLogsDelete;
    }
}
