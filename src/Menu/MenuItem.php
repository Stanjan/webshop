<?php

namespace App\Menu;

/**
 * Base implementation of \App\Menu\MenuItemInterface::class.
 */
class MenuItem extends Menu implements MenuItemInterface
{
    /**
     * @param array<MenuItemInterface> $children
     */
    public function __construct(
        private string $identifier,
        private string $label,
        private string $uri,
        private bool $isActive = false,
        private ?string $target = null,
        private ?string $icon = null,
        private ?string $translationDomain = null,
        array $children = [],
    ) {
        parent::__construct($children);
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function isActive(): bool
    {
        return $this->isActive;
    }

    public function getTarget(): ?string
    {
        return $this->target;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    public function getTranslationDomain(): ?string
    {
        return $this->translationDomain;
    }
}
