<?php

namespace App\DataFixtures;

use App\Entity\VatRate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

/**
 * Fixtures for \App\Entity\Admin\VatRate::class.
 */
class VatRateFixtures extends Fixture implements FixtureGroupInterface
{
    const REFERENCE_HIGH = 'vat_rate_high';
    const REFERENCE_LOW = 'vat_rate_low';

    public static function getGroups(): array
    {
        return [
            FixtureGroupInterface::WEBSHOP,
            FixtureGroupInterface::WEBSHOP_PRODUCT,
            FixtureGroupInterface::WEBSHOP_VAT_RATE,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $highVatRate = new VatRate();
        $highVatRate->setName('High');
        $highVatRate->setPercentage(21);
        $manager->persist($highVatRate);
        $this->setReference(self::REFERENCE_HIGH, $highVatRate);

        $lowVatRate = new VatRate();
        $lowVatRate->setName('Low');
        $lowVatRate->setPercentage(9);
        $manager->persist($lowVatRate);
        $this->setReference(self::REFERENCE_LOW, $lowVatRate);

        $manager->flush();
    }
}
