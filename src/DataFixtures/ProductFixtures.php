<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\VatRate;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Faker\ORM\Doctrine\Populator;

/**
 * Fixtures for \App\Entity\Product::class.
 */
class ProductFixtures extends Fixture implements FixtureGroupInterface, DependentFixtureInterface
{
    private Generator $generator;

    const REFERENCE_CONCEPT = 'product_concept';
    const REFERENCE_PUBLISHED = 'product_published';
    const REFERENCE_ARCHIVED = 'product_archived';

    public static function getGroups(): array
    {
        return [
            FixtureGroupInterface::ADMIN,
            FixtureGroupInterface::WEBSHOP,
            FixtureGroupInterface::WEBSHOP_PRODUCT,
        ];
    }

    public function getDependencies(): array
    {
        return [
            VatRateFixtures::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        /** @var VatRate $highVatRate */
        $highVatRate = $this->getReference(VatRateFixtures::REFERENCE_HIGH);
        /** @var VatRate $lowVatRate */
        $lowVatRate = $this->getReference(VatRateFixtures::REFERENCE_LOW);

        $conceptProduct = new Product();
        $conceptProduct->setSku('CONCEPT-01')
            ->setBarcode('7482467529349')
            ->setName('Concept foo bar')
            ->setDescription('Concept for a juicy foo bar rich of proteins!')
            ->setSlug('concept-foo-bar')
            ->setPurchasePrice(5.25)
            ->setSalesPrice(6.99)
            ->setStatus(Product::STATUS_CONCEPT)
            ->setAvailableFrom(new \DateTimeImmutable('+1 week'))
            ->setAvailableTo(new \DateTimeImmutable('+1 year'))
            ->setCanPreorder(true)
            ->setWeight(11.11)
            ->setStock(11)
            ->setVatRate($highVatRate)
        ;
        $manager->persist($conceptProduct);
        $this->setReference(self::REFERENCE_CONCEPT, $conceptProduct);

        $publishedProduct = new Product();
        $publishedProduct->setSku('PUBLISHED-01')
            ->setBarcode('9389405486895')
            ->setName('Foo bar')
            ->setDescription('A juicy foo bar rich of proteins!')
            ->setSlug('foo-bar')
            ->setPurchasePrice(8.65)
            ->setSalesPrice(11.11)
            ->setStatus(Product::STATUS_PUBLISHED)
            ->setAvailableFrom(new \DateTimeImmutable('11-11-2011'))
            ->setAvailableTo(null)
            ->setCanPreorder(false)
            ->setWeight(1.1)
            ->setStock(111)
            ->setVatRate($highVatRate)
        ;
        $manager->persist($publishedProduct);
        $this->setReference(self::REFERENCE_PUBLISHED, $publishedProduct);

        $archivedProduct = new Product();
        $archivedProduct->setSku('ARCHIVED-01')
            ->setBarcode('5589485853820')
            ->setName('Archived foo bar')
            ->setDescription('Archived: A juicy foo bar rich of proteins!')
            ->setSlug('archived-foo-bar')
            ->setPurchasePrice(8.65)
            ->setSalesPrice(11.11)
            ->setStatus(Product::STATUS_ARCHIVED)
            ->setAvailableFrom(null)
            ->setAvailableTo(new \DateTimeImmutable('now'))
            ->setCanPreorder(false)
            ->setWeight(1.11)
            ->setStock(0)
            ->setVatRate($lowVatRate)
        ;
        $manager->persist($archivedProduct);
        $this->setReference(self::REFERENCE_ARCHIVED, $archivedProduct);

        // Add 100 fake products.
        $this->generator = Factory::create();
        $populator = new Populator($this->generator, $manager);

        $populator->addEntity(Product::class, 100, [
            'sku' => function () {
                return substr($this->generator->unique()->uuid, 0, $this->generator->numberBetween(8, 12));
            },
            'barcode' => function () {
                return $this->generator->unique()->ean13;
            },
            'name' => function () {
                /** @var string $name */
                $name = $this->generator->words($this->generator->numberBetween(1, 3), true);

                return ucwords($name);
            },
            'description' => function () {
                return $this->generator->optional(0.9)->text();
            },
            'slug' => function () {
                return $this->generator->unique()->slug;
            },
            'purchasePrice' => function () {
                return $this->generator->randomFloat(2, 0, 100);
            },
            'salesPrice' => function () {
                return $this->generator->randomFloat(2, 0.01, 150);
            },
            'status' => function () {
                return $this->generator->randomElement([
                    Product::STATUS_CONCEPT,
                    Product::STATUS_PUBLISHED,
                    Product::STATUS_ARCHIVED,
                ]);
            },
            'availableFrom' => function () {
                return $this->generator->optional(0.9)->dateTimeThisYear();
            },
            'availableTo' => function () {
                return $this->generator->optional(0.6)->dateTimeThisYear();
            },
            'canPreorder' => function () {
                return $this->generator->boolean(50);
            },
            'weight' => function () {
                return $this->generator->optional(0.6)->randomFloat(2, 0, 100);
            },
            'stock' => function () {
                return $this->generator->randomFloat(2, 0, 100);
            },
            'vatRate' => function () {
                $reference = $this->generator->randomElement([
                    VatRateFixtures::REFERENCE_LOW,
                    VatRateFixtures::REFERENCE_HIGH,
                ]);

                return $this->getReference($reference);
            },
        ]);

        $populator->execute();

        $manager->flush();
    }
}
